//! Implementation of the CIEDE2000 colour difference calculation.

/// Calculate the CIEDE2000 distance between the two given L*a*b* colours.
/// This algorithm is implemented as described on <http://www.brucelindbloom.com>
/// Params:
/// lab_a -- tuple of (L, a, b) values.
/// lab_b -- tuple of (L, a, b) values.
///
/// Returns: colour difference as a float.
pub fn ciede2000(lab_a: (f32, f32, f32), lab_b: (f32, f32, f32)) -> f32 {

    let (l1, a1, b1) = lab_a;
    let (l2, a2, b2) = lab_b;

    let avg_lp = (l1 + l2) / 2.0;

    let c1 = hypot(a1, b1);
    let c2 = hypot(a2, b2);
    let c_delta = (c1 + c2) / 2.0;

    let g = 0.5 * (1.0 - (c_delta.powi(7) / (c_delta.powi(7) + (25_f32).powi(7))).sqrt());
    let a1p = (1.0 + g) * a1;
    let a2p = (1.0 + g) * a2;

    let c1p = hypot(a1p, b1);
    let c2p = hypot(a2p, b2);

    let avg_c1p_c2p = (c1p + c2p) / 2.0;

    let mut h1p = b1.atan2(a1p).to_degrees();

    if h1p < 0.0 {
        h1p += 360.0;
    }

    let mut h2p = b2.atan2(a2p).to_degrees();
    if h2p < 0.0 {
        h2p += 360.0;
    }

    let avg_hp = (if (h1p - h2p).abs() > 180.0 { 360.0 } else { 0.0 } + h1p + h2p) / 2.0;

    let t =
        1.0
            - 0.17 * (avg_hp - 30.0).to_radians().cos()
            + 0.24 * (2.0 * avg_hp).to_radians().cos()
            + 0.32 * (3.0 * avg_hp + 6.0).to_radians().cos()
            - 0.2 * (4.0 * avg_hp - 63.0).to_radians().cos()
    ;

    let diff_h2p_h1p = h2p - h1p;
    let mut delta_hp = diff_h2p_h1p + if diff_h2p_h1p.abs() > 180.0 { 360.0 } else { 0.0 };
    if h2p > h1p {
        delta_hp -= 720.0;
    }
    let delta_lp = l2 - l1;
    let delta_cp = c2p - c1p;
    delta_hp = 2.0 * (c2p * c1p).sqrt() * delta_hp.to_radians().sin() / 2.0;

    let s_l = 1.0 + ((0.015 * (avg_lp - 50.0).powi(2)) / (20.0 + (avg_lp - 50.0).powi(2)).sqrt());
    let s_c = 1.0 + 0.045 * avg_c1p_c2p;
    let s_h = 1.0 + 0.015 * avg_c1p_c2p * t;

    let delta_ro = 30.0 * (-(((avg_hp - 275.0) / 25.0).powi(2))).exp();
    let r_c = ((avg_c1p_c2p.powi(7)) / (avg_c1p_c2p.powi(7) + 25_f32.powi(7))).sqrt();
    let r_t = -2.0 * r_c * (2.0 * delta_ro.to_radians()).sin();

    let kl = 1.0;
    let kc = 1.0;
    let kh = 1.0;

    (
        (delta_lp / (s_l * kl)).powi(2)
            + (delta_cp / (s_c * kc)).powi(2)
            + (delta_hp / (s_h * kh)).powi(2)
            + r_t * (delta_cp / (s_c * kc)) * (delta_hp / (s_h * kh))
    ).sqrt()

}

fn hypot(a: f32, b: f32) -> f32 {
    (a.powi(2) + b.powi(2)).sqrt()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ciede2000() {
        let lab_a = (50.24, 22.66, -35.56);
        let lab_b = (55.15, 26.02, -27.71);
        let result = ciede2000(lab_a, lab_b);

        assert_eq!((result * 100.0).round() / 100.0, 7.56);
    }
}

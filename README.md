# CIEDE2000

An implementation of the CIEDE2000 colour difference calculation.

Example usage:
```rust 
use ciede2000::ciede2000;

fn main() {
    let lab_a = (50.24, 22.66, -35.56);
    let lab_b = (55.15, 26.02, -27.71);
    let result = ciede2000(lab_a, lab_b);
}
```

See https://landreville.gitlab.io/ciede2000 for documentation.